/**
 * @file
 * Adds tab focus handling for ajax inline-entity forms
 *
 * from https://www.drupal.org/project/inline_entity_form/issues/2998189#comment-13018029
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.focusInlineEntityForms = {
    attach: function (context) {
      $(".field--widget-inline-entity-form-complex .button.js-form-submit").on("mousedown",function() {
        var fieldwrapper = $(this).closest(".field--widget-inline-entity-form-complex");
        $(document).ajaxComplete(function() {
          var firstInput = fieldwrapper.find("fieldset input.form-text, fieldset input.entity-browser-processed")[0];
          if (typeof firstInput === "undefined") {
            firstInput = fieldwrapper.find("input")[0];
          }
          firstInput.focus();
        });
      });
    }
  };

})(jQuery, Drupal);
